package test;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Triwit
 */
public class TestWriteFile {

    public static void main(String[] triwit) throws FileNotFoundException, IOException {
        Player o = new Player('O');
        Player x = new Player('X');
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        o.win();
        o.lose();
        o.draw();
        x.win();
        x.lose();
        x.draw();
        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos != null) {
                    oos.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
